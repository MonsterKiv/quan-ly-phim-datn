# Đồ án tốt nghiệp - Quản lý rạp phim

Environment
-----------
- Laravel: 5.7

Setup Environment on Windows
----
_1. Open Powershell at xampp/htdocs folder_

_2. clone project: git clone https://gitlab.com/MonsterKiv/quan-ly-phim-datn.git quan_ly_rap_phim_

_3. cd quan_ly_rap_phim_

_4. cp env-example .env_

_5. Setting info connect DB in file .env of laravel_
        
        DB_CONNECTION=mysql
        
        DB_HOST=127.0.0.1
        
        DB_PORT=3306
        
        DB_DATABASE=quan_ly_rap_phim
        
        DB_USERNAME=root
        
        DB_PASSWORD=

        ===============

        BASE_URL=http://localhost:8080/quan_ly_phim/public/

_6. composer install_

_7. php artisan key:generate_
        
_8. php artisan migrate_
