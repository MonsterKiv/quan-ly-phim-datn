<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOriginDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('avatar')->nullable();
            $table->string('name');
            $table->date('birthday');
            $table->integer('gender');
            $table->string('address');
            $table->integer('role');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('movie', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origin_name');
            $table->string('translated_name');
            $table->string('genre');
            $table->string('director');
            $table->string('country');
            $table->string('actors');
            $table->integer('duration');
            $table->text('available_format');
            $table->date('release_date')->nullable();
            $table->date('close_date')->nullable();
            $table->text('description')->nullable();
            $table->string('tags')->nullable();
            $table->text('vertical_thumbnail_image')->nullable();
            $table->text('horizontal_thumbnail_image')->nullable();
            $table->text('youtube_trailer')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('show', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('movie_id');
            $table->unsignedInteger('cinema_id');
            $table->unsignedInteger('format_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cinema', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('prefecture');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('movie_format', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('discount_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('discount');
            $table->integer('repeat_type');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('weekday');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('received_ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guest_name');
            $table->string('guest_address');
            $table->string('guest_phone');
            $table->string('guest_email');
            $table->string('guest_desired_area');
            $table->integer('price');
            $table->integer('discount');
            $table->unsignedInteger('show_id');
            $table->integer('status');
            $table->dateTime('ticket_time');
            $table->string('rejected_reason');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('register_mail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guest_name');
            $table->string('email');
            $table->text('followed_movie');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('access_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address');
            $table->integer('flag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('movie');
        Schema::dropIfExists('show');
        Schema::dropIfExists('cinema');
        Schema::dropIfExists('movie_format');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('discount_plan');
        Schema::dropIfExists('received_ticket');
        Schema::dropIfExists('register_mail');
        Schema::dropIfExists('access_log');
    }
}
